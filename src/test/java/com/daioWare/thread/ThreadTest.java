package com.daioWare.thread;

import com.daioware.thread.ThreadPool;

public class ThreadTest {

	static Runnable getRunnable(String s,int millis) {
		return new Runnable() {
			@Override
			public synchronized void run() {
				System.out.println(s);
				try {
					wait(millis);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(s +" finished");
			}
		};
	}
	public static void test() {
		ThreadPool pool=new ThreadPool();
		String nameTemplate="Thread ";
		for(int i=0;i<10;i++) {
			System.out.println("Adding "+nameTemplate+" "+(i+1));
			pool.exec(getRunnable(nameTemplate+" "+(i+1)+" running...",2*1000));
		}
		pool.waitForThreads();
		System.out.println("FINISHED");
	}
	public static void main(String[] args) {
		test();
	}

}
