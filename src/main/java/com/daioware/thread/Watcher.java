package com.daioware.thread;

public class Watcher<T> extends Thread{
	protected T value;
	private boolean active=true;
	private Runnable target;
	
	public Watcher(Runnable target) {
		this.target = target;
	}
	public T getValue() {
		return value;
	}
	public synchronized void setValue(T v) {
		value=v;
		notify();
	}
	
	public Runnable getTarget() {
		return target;
	}
	public void setTarget(Runnable target) {
		this.target = target;
	}
	public boolean isActive() {
		return active;
	}
	public synchronized void setActive(boolean active) {
		this.active = active;
		if(!active) {
			notify();
		}
	}
	public synchronized void run() {
		while(isActive()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(isActive()) {
				new Thread(getTarget()).start();
			}
		}
	}
}
