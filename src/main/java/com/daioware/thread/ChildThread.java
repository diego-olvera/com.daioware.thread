package com.daioware.thread;

/**
 * Thread to execute under the escope of a ThreadPool
 * @author Diego Olvera
 *
 */
public class ChildThread implements Runnable{
	private ThreadPool father;
	private Runnable runnable;
	
	public ChildThread(ThreadPool father,Runnable runnable) {
		this.father=father;
		this.runnable=runnable;
	}
	
	public void run() {
		try {
			runnable.run();
		}catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			father.removeRunningThread(this);
		}
	}
}
